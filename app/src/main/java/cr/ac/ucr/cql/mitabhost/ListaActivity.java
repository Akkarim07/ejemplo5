package cr.ac.ucr.cql.mitabhost;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;


public class ListaActivity extends Fragment {
    private ListView listView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
// Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_lista, container, false);
        listView = (ListView)view.findViewById(R.id.lista);
        ArrayList<String> lista = new ArrayList<String>();
        lista.add("Element1");
        lista.add("Element2");
        lista.add("Element3");
        ArrayAdapter<String> allItemsAdapter =
                new ArrayAdapter<String>(getActivity().getBaseContext(),
                        android.R.layout.simple_list_item_1, lista);
        listView.setAdapter(allItemsAdapter);
// ListView Item Click Listener para las llamadas a las opciones de los items
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
// ListView Clicked item index
                int itemPosition = position;
// ListView Clicked item value
                String itemValue = (String)listView.getItemAtPosition(position);
// Show Alert
                Toast.makeText(getActivity(), "Position: " + itemPosition +
                        " ListItem: " + itemValue, Toast.LENGTH_LONG).show();
            }
        });
        return view;
    }
}